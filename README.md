# ![logo](http://savepic.ru/14284527.jpg) **"All Music Stuff"**

---
## Online shop of musical instruments!

This is the source code repository for the online shop "All Music Stuff". It is just a project designed to train our skills in Django.

## Features:
* Intuitive Web Design
* и т.д.

## Specifications:
* Django
* PostgreSQL

## Contacts
For further questions please contact <pestibyen@gmail.com>